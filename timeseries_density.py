import boto3
import argparse
import pyspark.sql.functions as F
import json
from io import StringIO
from pyspark.sql.window import Window
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType

S3_RANGES_BUCKET = "anastasia-emr-scripts-dev-bucket-rparra"


def check_density(timeseries, periodicity, target_column, date_column,
                  item_columns):
    s3 = boto3.resource('s3')
    with SparkSession.builder \
        .appName("TimseriesDensity") \
        .getOrCreate() as spark:
        timeseries_list = []

        uri = '/'.join(timeseries.split('/')[3:-2])
        for object_summary in s3.Bucket(
            timeseries.split('/')[2]).objects.filter(
            Prefix=f'{uri}'):
            if '.csv' in object_summary.key and 'density' not in object_summary.key:
                print(object_summary.key)
                timeseries_list.append(
                    's3://{}{}'.format('/'.join(timeseries.split('/')[2:]),
                                       object_summary.key.split('/')[-1]))

        df_timeseries = None
        print(timeseries_list)
        for filepath in timeseries_list:
            df_file = spark \
                .read \
                .option('mode', 'PERMISSIVE') \
                .csv(filepath, inferSchema=True, header=True)
            if df_timeseries:
                df_timeseries = df_timeseries.union(df_file)
            else:
                df_timeseries = df_file

        # renaming columns
        df_timeseries = df_timeseries.withColumnRenamed(date_column, 'date') \
            .withColumnRenamed(target_column, 'target')
        index = 0
        col_list = []
        for col_str in item_columns.split('%'):
            col_name = col_str.split(',')
            df_timeseries = df_timeseries.withColumnRenamed(col_name[0],
                                                            f'item_{str(index)}')
            col_list.append(f'item_{str(index)}')
            index += 1

        # concat item col

        df_timeseries = df_timeseries.withColumn('item',
                                                 F.concat_ws('|', *col_list))

        # get ranges
        content_object = s3.Object(S3_RANGES_BUCKET, 'timeseries-density/ranges/density_ranges.json')
        file_content = content_object.get()['Body'].read().decode('utf-8')
        density_ranges = json.loads(file_content)

        rows_number = density_ranges[periodicity]["data_quantity"]
        #print(rows_number)

        # df partition
        windowDept = Window.partitionBy("item").orderBy(F.col("date").desc())
        df_timeseries = df_timeseries.withColumn("row", F.row_number().over(
            windowDept))
        df_timeseries = df_timeseries.filter(F.col("row") <= rows_number)

        # calculate density

        def check_density(density):
            ranges = density_ranges[periodicity]
            if density == 0:
                return ranges["no_data_label"]
            for r in ranges["ranges"]:
                if density > r["min"] and density <= r["max"]:
                    return r["label"]
            return "unavailable value"

        spark.udf.register('density', check_density)

        cnt_cond = lambda cond: F.sum(F.when(cond, 1).otherwise(0))
        df_timeseries = df_timeseries.groupBy('item').agg(
            cnt_cond(F.col('target') != 0).alias('non_zero'))
        df_timeseries = df_timeseries.withColumn('density',
                                                 F.expr("density(non_zero)"))

        # save on s3
        csv_buffer = StringIO()
        df_timeseries.toPandas().to_csv(csv_buffer, index=False)
        filename = f'{uri}/timeSeriesDensity/density.csv'
        s3.Object(timeseries.split('/')[2], filename).put(
            Body=csv_buffer.getvalue())
        df_timeseries.show(48, False)
        print("job ended")



if __name__ == "__main__":
    # Get console arguments and pass them to the main validation function
    parser = argparse.ArgumentParser()
    # timeseries_uri
    parser.add_argument('--timseries_info',
                        help="Time series URI to be calculated")
    # periodicity
    parser.add_argument('--periodicity',
                        help="Set the time span in wich the time series density will be calculated")
    parser.add_argument('--target_column',
                        help="Name of the column that contains the target value")
    parser.add_argument('--date_column',
                        help="Name of the column that contains the date value")
    parser.add_argument('--item_columns',
                        help="Name of the column that contains the items values")
    args = parser.parse_args()

    print("Starting timeseries density calculation job")
    check_density(args.timseries_info, args.periodicity, args.target_column,
                  args.date_column, args.item_columns)
